package Sistema;


import Conexion.MetodosBD;
import Conexion.PoolConexionMySQL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Emanuel Garcia
 */
public class Inventario extends javax.swing.JFrame {

    /**
     * Creates new form Inventario
     */
    
    public Inventario() {
        initComponents();
        setLocationRelativeTo(null);
        try{
            //Para ver ciertas columas, solo teclea el nombre de las que se vean
            String SSQL = "SELECT ID_Inventario, Nombre_Producto, Existencia FROM inventario";
            Connection conect;
            conect=metodospool.dataSource.getConnection();
            PreparedStatement st = conect.prepareStatement(SSQL);
            ResultSet rst=st.executeQuery(SSQL);
            ResultSetMetaData rsMt=rst.getMetaData();
            int numerocolumnas=rsMt.getColumnCount();

            DefaultTableModel modelo=new DefaultTableModel();
            this.jTable5.setModel(modelo);
            this.jTable6.setModel(modelo);
            for(int i=1;i<=numerocolumnas;i++){
                modelo.addColumn(rsMt.getColumnLabel(i));
            }            
            while(rst.next()){
                Object[] fila=new Object[numerocolumnas];
                for(int i=0;i<numerocolumnas;i++){
                    fila[i]=rst.getObject(i+1);
                }

                modelo.addRow(fila);
            }

        }catch(SQLException e){
            JOptionPane.showMessageDialog(null,e.getMessage());
        }
        Ingreso.setText("2");
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel6 = new javax.swing.JLabel();
        Regresar = new javax.swing.JButton();
        jLabel10 = new javax.swing.JLabel();
        jTabbedPane3 = new javax.swing.JTabbedPane();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTable2 = new javax.swing.JTable();
        BotonVer = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox<>();
        Nombre = new javax.swing.JTextField();
        BotonBuscar = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();
        Existencia = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        NombreProducto = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        Ingreso = new javax.swing.JTextField();
        BotonInsertar = new javax.swing.JButton();
        jPanel8 = new javax.swing.JPanel();
        jScrollPane6 = new javax.swing.JScrollPane();
        jTable6 = new javax.swing.JTable();
        BotonActualizar1 = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        jPanel7 = new javax.swing.JPanel();
        BotonActualizar = new javax.swing.JButton();
        jScrollPane5 = new javax.swing.JScrollPane();
        jTable5 = new javax.swing.JTable();
        buscar = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        NombreProducto2 = new javax.swing.JTextField();
        BotonModificar = new javax.swing.JButton();
        Existencia2 = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel6.setFont(new java.awt.Font("Century Gothic", 2, 48)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("Inventario");
        getContentPane().add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 10, 250, 80));

        Regresar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Salir.png"))); // NOI18N
        Regresar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        Regresar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RegresarActionPerformed(evt);
            }
        });
        getContentPane().add(Regresar, new org.netbeans.lib.awtextra.AbsoluteConstraints(830, 580, 100, 90));

        jLabel10.setFont(new java.awt.Font("Century Gothic", 2, 24)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(255, 255, 255));
        jLabel10.setText("Regresar");
        jLabel10.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        getContentPane().add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(830, 670, 110, 30));

        jPanel3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jTable2.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        jTable2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID_Inventario", "Nombre_Producto", "Existencia", "ID_Ingreso"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(jTable2);

        jPanel3.add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 0, 910, 330));

        BotonVer.setFont(new java.awt.Font("Century Gothic", 2, 36)); // NOI18N
        BotonVer.setText("Ver Tabla");
        BotonVer.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        BotonVer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotonVerActionPerformed(evt);
            }
        });
        jPanel3.add(BotonVer, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 340, 260, 60));

        jTabbedPane3.addTab("Ver Inventario", jPanel3);

        jPanel4.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel2.setFont(new java.awt.Font("Century Gothic", 2, 36)); // NOI18N
        jLabel2.setText("Seleccione por donde comenzar a buscar: ");
        jPanel4.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 40, -1, -1));

        jComboBox1.setFont(new java.awt.Font("Century Gothic", 2, 36)); // NOI18N
        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Nombre_Producto", "Existencia" }));
        jComboBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox1ActionPerformed(evt);
            }
        });
        jPanel4.add(jComboBox1, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 100, -1, -1));

        Nombre.setFont(new java.awt.Font("Century Gothic", 2, 36)); // NOI18N
        jPanel4.add(Nombre, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 200, 810, -1));

        BotonBuscar.setFont(new java.awt.Font("Century Gothic", 2, 36)); // NOI18N
        BotonBuscar.setText("Buscar");
        BotonBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotonBuscarActionPerformed(evt);
            }
        });
        jPanel4.add(BotonBuscar, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 300, 190, 80));

        jTabbedPane3.addTab("Buscar", jPanel4);

        jPanel5.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        Existencia.setFont(new java.awt.Font("Century Gothic", 2, 36)); // NOI18N
        jPanel5.add(Existencia, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 130, 200, -1));

        jLabel3.setFont(new java.awt.Font("Century Gothic", 2, 36)); // NOI18N
        jLabel3.setText("Existencia:");
        jPanel5.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 140, -1, -1));

        jLabel4.setFont(new java.awt.Font("Century Gothic", 2, 36)); // NOI18N
        jLabel4.setText("Nombre de Producto:");
        jPanel5.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 30, -1, -1));

        NombreProducto.setFont(new java.awt.Font("Century Gothic", 2, 36)); // NOI18N
        jPanel5.add(NombreProducto, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 30, 470, -1));

        jLabel5.setFont(new java.awt.Font("Century Gothic", 2, 36)); // NOI18N
        jLabel5.setText("ID:");
        jPanel5.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 220, -1, -1));

        Ingreso.setFont(new java.awt.Font("Century Gothic", 2, 36)); // NOI18N
        jPanel5.add(Ingreso, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 210, 200, -1));

        BotonInsertar.setFont(new java.awt.Font("Century Gothic", 2, 36)); // NOI18N
        BotonInsertar.setText("Insertar");
        BotonInsertar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotonInsertarActionPerformed(evt);
            }
        });
        jPanel5.add(BotonInsertar, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 310, 190, 80));

        jTabbedPane3.addTab("Añadir Producto", jPanel5);

        jPanel8.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jTable6.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        jTable6.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID_Inventario", "Nombre_Producto", "Existencia", "ID_Ingreso"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable6MouseClicked(evt);
            }
        });
        jScrollPane6.setViewportView(jTable6);

        jPanel8.add(jScrollPane6, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 920, 270));

        BotonActualizar1.setFont(new java.awt.Font("Century Gothic", 2, 36)); // NOI18N
        BotonActualizar1.setText("Eliminar");
        BotonActualizar1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotonActualizar1ActionPerformed(evt);
            }
        });
        jPanel8.add(BotonActualizar1, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 350, 240, 60));

        jLabel7.setFont(new java.awt.Font("Century Gothic", 2, 36)); // NOI18N
        jLabel7.setText("De la tabla seleccione el registro a eliminar:");
        jPanel8.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 280, -1, -1));

        jTabbedPane3.addTab("Eliminar Producto", jPanel8);

        jPanel7.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        BotonActualizar.setFont(new java.awt.Font("Century Gothic", 2, 36)); // NOI18N
        BotonActualizar.setText("Actualizar");
        BotonActualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotonActualizarActionPerformed(evt);
            }
        });
        jPanel7.add(BotonActualizar, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 360, 240, 60));

        jTable5.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        jTable5.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID_Inventario", "Nombre_Producto", "Existencia", "ID_Ingreso"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable5MouseClicked(evt);
            }
        });
        jScrollPane5.setViewportView(jTable5);

        jPanel7.add(jScrollPane5, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 0, 920, 140));

        buscar.setFont(new java.awt.Font("Century Gothic", 2, 36)); // NOI18N
        buscar.setEnabled(false);
        buscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buscarActionPerformed(evt);
            }
        });
        jPanel7.add(buscar, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 300, 200, -1));

        jLabel8.setFont(new java.awt.Font("Century Gothic", 2, 36)); // NOI18N
        jLabel8.setText("Existencia:");
        jPanel7.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 230, -1, -1));

        jLabel9.setFont(new java.awt.Font("Century Gothic", 2, 36)); // NOI18N
        jLabel9.setText("Nombre de Producto:");
        jPanel7.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 160, -1, -1));

        NombreProducto2.setFont(new java.awt.Font("Century Gothic", 2, 36)); // NOI18N
        jPanel7.add(NombreProducto2, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 160, 470, -1));

        BotonModificar.setFont(new java.awt.Font("Century Gothic", 2, 36)); // NOI18N
        BotonModificar.setText("Modificar");
        BotonModificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BotonModificarActionPerformed(evt);
            }
        });
        jPanel7.add(BotonModificar, new org.netbeans.lib.awtextra.AbsoluteConstraints(590, 360, 240, 60));

        Existencia2.setFont(new java.awt.Font("Century Gothic", 2, 36)); // NOI18N
        Existencia2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Existencia2ActionPerformed(evt);
            }
        });
        jPanel7.add(Existencia2, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 230, 200, -1));

        jTabbedPane3.addTab("Modificar Producto", jPanel7);

        getContentPane().add(jTabbedPane3, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 80, 940, 460));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Interfaz.PNG"))); // NOI18N
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 960, 720));

        pack();
    }// </editor-fold>//GEN-END:initComponents
PoolConexionMySQL metodospool=new PoolConexionMySQL();
    private void BotonVerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotonVerActionPerformed
        try{
            //Para ver ciertas columas, solo teclea el nombre de las que se vean
            String SSQL = "SELECT ID_Inventario, Nombre_Producto, Existencia FROM inventario";
            Connection conect;
            conect=metodospool.dataSource.getConnection();
            PreparedStatement st = conect.prepareStatement(SSQL);
            ResultSet rst=st.executeQuery(SSQL);
            ResultSetMetaData rsMt=rst.getMetaData();
            int numerocolumnas=rsMt.getColumnCount();

            DefaultTableModel modelo=new DefaultTableModel();
            this.jTable2.setModel(modelo);
            jTable5.setModel(modelo);
            jTable6.setModel(modelo);
            for(int i=1;i<=numerocolumnas;i++){
                modelo.addColumn(rsMt.getColumnLabel(i));
            }            
            while(rst.next()){
                Object[] fila=new Object[numerocolumnas];
                for(int i=0;i<numerocolumnas;i++){
                    fila[i]=rst.getObject(i+1);
                }

                modelo.addRow(fila);
            }

        }catch(SQLException e){
            JOptionPane.showMessageDialog(null,e.getMessage());
        }
    }//GEN-LAST:event_BotonVerActionPerformed

    private void RegresarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_RegresarActionPerformed
        Administrador a=new Administrador();
        a.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_RegresarActionPerformed
    MetodosBD mbd=new MetodosBD();
    private void BotonBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotonBuscarActionPerformed
        mbd.Buscari(this.Nombre.getText(),jComboBox1.getSelectedItem().toString(), jTable2);
    }//GEN-LAST:event_BotonBuscarActionPerformed
    
    private void BotonInsertarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotonInsertarActionPerformed
        try {
            String SSQL = "INSERT INTO inventario(Nombre_Producto,Existencia,Inversion_ID_Inversion) VALUES(?,?,?)";
            Connection conect;
            conect=metodospool.dataSource.getConnection();
            
            PreparedStatement st = conect.prepareStatement(SSQL);
            st.setString(1,NombreProducto.getText());
            st.setString(2,Existencia.getText());
            st.setString(3,Ingreso.getText());
            st.executeUpdate();
            JOptionPane.showMessageDialog(null,"Los datos han sido guardados");
        } catch (SQLException ex) {
            Logger.getLogger(Inventario.class.getName()).log(Level.SEVERE, null, ex);
        }
            
    }//GEN-LAST:event_BotonInsertarActionPerformed

    private void buscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buscarActionPerformed
        
    }//GEN-LAST:event_buscarActionPerformed

    private void BotonActualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotonActualizarActionPerformed
        try {
            String SSQL = "UPDATE inventario SET Nombre_Producto='"+NombreProducto2.getText()+"', Existencia='"+Existencia2.getText()+"' WHERE ID_Inventario='"+buscar.getText()+"'";
            Connection conect;
            conect=metodospool.dataSource.getConnection();
            PreparedStatement st = conect.prepareStatement(SSQL);
            st.executeUpdate();
            JOptionPane.showMessageDialog(null,"Los datos han sido actualizados");
            NombreProducto2.setText("");
            Existencia2.setText("");
            buscar.setText("");
        } catch (SQLException ex) {
            Logger.getLogger(Inventario.class.getName()).log(Level.SEVERE, null, ex);
        }
        try{
            //Para ver ciertas columas, solo teclea el nombre de las que se vean
            String SSQL = "SELECT ID_Inventario, Nombre_Producto, Existencia FROM inventario";
            Connection conect;
            conect=metodospool.dataSource.getConnection();
            PreparedStatement st = conect.prepareStatement(SSQL);
            ResultSet rst=st.executeQuery(SSQL);
            ResultSetMetaData rsMt=rst.getMetaData();
            int numerocolumnas=rsMt.getColumnCount();

            DefaultTableModel modelo=new DefaultTableModel();
            this.jTable5.setModel(modelo);
            for(int i=1;i<=numerocolumnas;i++){
                modelo.addColumn(rsMt.getColumnLabel(i));
            }            
            while(rst.next()){
                Object[] fila=new Object[numerocolumnas];
                for(int i=0;i<numerocolumnas;i++){
                    fila[i]=rst.getObject(i+1);
                }

                modelo.addRow(fila);
            }

        }catch(SQLException e){
            JOptionPane.showMessageDialog(null,e.getMessage());
        }
    }//GEN-LAST:event_BotonActualizarActionPerformed

    private void jTable5MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable5MouseClicked
          
    }//GEN-LAST:event_jTable5MouseClicked

    private void BotonModificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotonModificarActionPerformed
        int fila=jTable5.getSelectedRow();
        if(fila>=0){
            buscar.setText(jTable5.getValueAt(fila,0).toString());
            NombreProducto2.setText(jTable5.getValueAt(fila,1).toString());
            Existencia2.setText(jTable5.getValueAt(fila,2).toString());
        }else{
            JOptionPane.showMessageDialog(null,"Fila no seleccionada");
        }
        
    }//GEN-LAST:event_BotonModificarActionPerformed

    private void Existencia2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Existencia2ActionPerformed
        
    }//GEN-LAST:event_Existencia2ActionPerformed

    private void jTable6MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable6MouseClicked
        
    }//GEN-LAST:event_jTable6MouseClicked

    private void BotonActualizar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BotonActualizar1ActionPerformed
        int fila=jTable6.getSelectedRow();
        String valor=jTable6.getValueAt(fila,0).toString();
        if(fila>=0){
            try {
            String SSQL = "DELETE FROM inventario WHERE ID_Inventario='"+valor+"'";
            Connection conect;
            conect=metodospool.dataSource.getConnection();
            PreparedStatement st = conect.prepareStatement(SSQL);
            st.executeUpdate();
            JOptionPane.showMessageDialog(null,"Los datos han sido eliminados");
            } catch (SQLException ex) {
                Logger.getLogger(Inventario.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        try{
            //Para ver ciertas columas, solo teclea el nombre de las que se vean
            String SSQL = "SELECT ID_Inventario, Nombre_Producto, Existencia FROM inventario";
            Connection conect;
            conect=metodospool.dataSource.getConnection();
            PreparedStatement st = conect.prepareStatement(SSQL);
            ResultSet rst=st.executeQuery(SSQL);
            ResultSetMetaData rsMt=rst.getMetaData();
            int numerocolumnas=rsMt.getColumnCount();

            DefaultTableModel modelo=new DefaultTableModel();
            this.jTable6.setModel(modelo);
            for(int i=1;i<=numerocolumnas;i++){
                modelo.addColumn(rsMt.getColumnLabel(i));
            }            
            while(rst.next()){
                Object[] filai = new Object[numerocolumnas];
                for(int i=0;i<numerocolumnas;i++){
                    filai[i]=rst.getObject(i+1);
                }

                modelo.addRow(filai);
            }

        }catch(SQLException e){
            JOptionPane.showMessageDialog(null,e.getMessage());
        }
    }//GEN-LAST:event_BotonActualizar1ActionPerformed

    private void jComboBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jComboBox1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Inventario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Inventario().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BotonActualizar;
    private javax.swing.JButton BotonActualizar1;
    private javax.swing.JButton BotonBuscar;
    private javax.swing.JButton BotonInsertar;
    private javax.swing.JButton BotonModificar;
    private javax.swing.JButton BotonVer;
    private javax.swing.JTextField Existencia;
    private javax.swing.JTextField Existencia2;
    private javax.swing.JTextField Ingreso;
    private javax.swing.JTextField Nombre;
    private javax.swing.JTextField NombreProducto;
    private javax.swing.JTextField NombreProducto2;
    private javax.swing.JButton Regresar;
    private javax.swing.JTextField buscar;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JTabbedPane jTabbedPane3;
    public javax.swing.JTable jTable2;
    public javax.swing.JTable jTable5;
    public javax.swing.JTable jTable6;
    // End of variables declaration//GEN-END:variables
}
