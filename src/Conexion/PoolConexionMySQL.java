package Conexion;

import java.sql.PreparedStatement;
import javax.sql.DataSource;
import org.apache.commons.dbcp.BasicDataSource;

public final class PoolConexionMySQL {
    //Conexion
    public static PreparedStatement Conexion;
    public DataSource dataSource;
    //Tener acceso a la bd
    public String db="lavanderiamariela";
    public String url="jdbc:mysql://localhost/"+db;
    public String user="root";
    public String pass="e1657a815";
    
    public PoolConexionMySQL(){
        iniciarDataSource();
    }
    
    public void iniciarDataSource(){
        BasicDataSource basicDataSource=new BasicDataSource();
        basicDataSource.setDriverClassName("org.gjt.mm.mysql.Driver");
        basicDataSource.setUsername(user);
        basicDataSource.setPassword(pass);
        basicDataSource.setUrl(url);
        //Establece limite de conexiones de usuarios a la bd
        basicDataSource.setMaxActive(10);
        dataSource=basicDataSource;
    }
}
