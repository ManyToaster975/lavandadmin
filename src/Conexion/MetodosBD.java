
package Conexion;

import Sistema.LoginEmpleado;
import Sistema.LoginAdministrador;
import java.awt.HeadlessException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class MetodosBD {
    PoolConexionMySQL metodospool= new PoolConexionMySQL();
    DefaultTableModel ModeloTabla;
    //LoginAdministrador
    public int valido(){
        //Administrador
        String clavea = LoginAdministrador.Clave.getText();
        String usuarioa = LoginAdministrador.Usuario.getText();
        String contraseñaa = String.valueOf(LoginAdministrador.Contraseña.getPassword());
        int resultado = 0;
        //Administrador
        String SSQLA = "SELECT * FROM trabajador WHERE Clave_Trabajador='"+clavea+"' AND Usuario='"+usuarioa+"' AND Contraseña='"+contraseñaa+"'";
        Connection conect=null;
        
            try {
                conect=metodospool.dataSource.getConnection();
                Statement st = conect.createStatement();
                ResultSet rsa = st.executeQuery(SSQLA);
                if(rsa.next()){
                    resultado=1;
                }
            } catch (SQLException ex) {
                //ERROR_MESSAGE.- Crea la ventana con el icono de error
                JOptionPane.showMessageDialog(null,ex,"Error de Conexion",JOptionPane.ERROR_MESSAGE);
            }finally{
                try {
                    conect.close();
                } catch (SQLException ex) {
                    JOptionPane.showMessageDialog(null,ex,"Error de Cierre",JOptionPane.ERROR_MESSAGE);
                }
            }
        
        return resultado;
    }
    
    //LoginEmpleado
    public int validacione(){
        //Empleado
        String clavee = LoginEmpleado.Clave.getText();
        String usuarioe = LoginEmpleado.Usuario.getText();
        String contraseñae = String.valueOf(LoginEmpleado.Contraseña.getPassword());
        
        int resultado = 0;
        //Empleado
        String SSQLE = "SELECT * FROM trabajador WHERE Clave_Trabajador='"+clavee+"' AND Usuario='"+usuarioe+"' AND Contraseña='"+contraseñae+"'";
        
        Connection conect=null;
        
            try {
                conect=metodospool.dataSource.getConnection();
                Statement st = conect.createStatement();
                ResultSet rse = st.executeQuery(SSQLE);
                if(rse.next()){
                    resultado=1;
                }
            } catch (SQLException ex) {
                //ERROR_MESSAGE.- Crea la ventana con el icono de error
                JOptionPane.showMessageDialog(null,ex,"Error de Conexion",JOptionPane.ERROR_MESSAGE);
            }finally{
                try {
                    conect.close();
                } catch (SQLException ex) {
                    JOptionPane.showMessageDialog(null,ex,"Error de Cierre",JOptionPane.ERROR_MESSAGE);
                }
            }
        
        return resultado;
    }
    
    //Buscar Clientes
    public void Buscar(String dato, String filtro, JTable tabla){
        String [] columnas={"Clave_Clientes","Nombre","Direccion","Telefono","Email"};
        String [] registros=new String[5];
        ModeloTabla=new DefaultTableModel(null,columnas);
        String SSQL = null;
        Connection conect=null;
        if(filtro.equals("Nombre")){
            SSQL="SELECT Clave_Clientes, Nombre, Direccion, Telefono, Email "
                + "FROM clientes WHERE Nombre LIKE '%"+dato+"%'";
        }else if(filtro.equals("Direccion")){
            SSQL="SELECT Clave_Clientes, Nombre, Direccion, Telefono, Email "
                + "FROM clientes WHERE Direccion LIKE '%"+dato+"%'";
        }
        try{
            conect=metodospool.dataSource.getConnection();
            PreparedStatement st = conect.prepareStatement(SSQL);
            ResultSet rs = st.executeQuery();
            while(rs.next()){
                //Hacer print para rectificar
                registros[0]=rs.getString("Clave_Clientes");
                System.out.println(rs.getString("Clave_Clientes"));
                registros[1]=rs.getString("Nombre");
                System.out.println(rs.getString("Nombre"));
                registros[2]=rs.getString("Direccion");
                System.out.println(rs.getString("Direccion"));
                registros[3]=rs.getString("Telefono");
                System.out.println(rs.getString("Telefono"));
                registros[4]=rs.getString("Email");
                System.out.println(rs.getString("Email"));
                ModeloTabla.addRow(registros);
            }
            tabla.setModel(ModeloTabla);
            JOptionPane.showMessageDialog(null,"Busqueda finalizada");
        }catch(SQLException | HeadlessException e){
            JOptionPane.showMessageDialog(null,e,"Error de conexion",JOptionPane.ERROR_MESSAGE);
        }finally{
            if(conect!=null){
               try{
                   conect.close();
               } catch(Exception ex){
                   JOptionPane.showMessageDialog(null,ex,"Error de cierre",JOptionPane.ERROR_MESSAGE);
               }
            }
        }
    }
    
    //Buscar Diario
    public void Buscard(String dato, String filtro, JTable tabla){
        String [] columnas={"No_Ventas","Fecha_Pedido","Fecha_Final","Cargas","Tipo_Servicio","Precio_Servicio","Estado_Entrega","Clientes_Clave_Clientes","Trabajador_Clave_Trabajador"};
        String [] registros=new String[9];
        ModeloTabla=new DefaultTableModel(null,columnas);
        String SSQL = null;
        Connection conect=null;
        switch (filtro) {
            case "Fecha_Pedido":
                SSQL="SELECT No_Ventas, Fecha_Pedido, Fecha_Final, Cargas, Tipo_Servicio, Precio_Servicio, Estado_Entrega, Clientes_Clave_Clientes, Trabajador_Clave_Trabajador "
                        + "FROM ventas WHERE Fecha_Pedido LIKE '%"+dato+"%'";
                break;
            case "Fecha_Final":
                SSQL="SELECT No_Ventas, Fecha_Pedido, Fecha_Final, Cargas, Tipo_Servicio, Precio_Servicio, Estado_Entrega, Clientes_Clave_Clientes, Trabajador_Clave_Trabajador "
                        + "FROM ventas WHERE Fecha_Final LIKE '%"+dato+"%'";
                break;
            case "Cargas":
                SSQL="SELECT No_Ventas, Fecha_Pedido, Fecha_Final, Cargas, Tipo_Servicio, Precio_Servicio, Estado_Entrega, Clientes_Clave_Clientes, Trabajador_Clave_Trabajador "
                        + "FROM ventas WHERE Cargas LIKE '%"+dato+"%'";
                break;
            case "Tipo_Servicio":
                SSQL="SELECT No_Ventas, Fecha_Pedido, Fecha_Final, Cargas, Tipo_Servicio, Precio_Servicio, Estado_Entrega, Clientes_Clave_Clientes, Trabajador_Clave_Trabajador "
                        + "FROM ventas WHERE Tipo_Servicio LIKE '%"+dato+"%'";
                break;
            case "Precio_Servicio":
                SSQL="SELECT No_Ventas, Fecha_Pedido, Fecha_Final, Cargas, Tipo_Servicio, Precio_Servicio, Estado_Entrega, Clientes_Clave_Clientes, Trabajador_Clave_Trabajador "
                        + "FROM ventas WHERE Precio_Servicio LIKE '%"+dato+"%'";
                break;
            case "Estado_Entrega":
                SSQL="SELECT No_Ventas, Fecha_Pedido, Fecha_Final, Cargas, Tipo_Servicio, Precio_Servicio, Estado_Entrega, Clientes_Clave_Clientes, Trabajador_Clave_Trabajador "
                        + "FROM ventas WHERE Estado_Entrega LIKE '%"+dato+"%'";
                break;
            case "Clave_Cliente":
                SSQL="SELECT No_Ventas, Fecha_Pedido, Fecha_Final, Cargas, Tipo_Servicio, Precio_Servicio, Estado_Entrega, Clientes_Clave_Clientes, Trabajador_Clave_Trabajador "
                        + "FROM ventas WHERE Clientes_Clave_Clientes LIKE '%"+dato+"%'";
                break;
            case "Clave_Trabajador":
                SSQL="SELECT No_Ventas, Fecha_Pedido, Fecha_Final, Cargas, Tipo_Servicio, Precio_Servicio, Estado_Entrega, Clientes_Clave_Clientes, Trabajador_Clave_Trabajador "
                        + "FROM ventas WHERE Trabajador_Clave_Trabajador LIKE '%"+dato+"%'";
                break;
            default:
                break;
        }
        try{
            conect=metodospool.dataSource.getConnection();
            PreparedStatement st = conect.prepareStatement(SSQL);
            ResultSet rs = st.executeQuery();
            while(rs.next()){
                registros[0]=rs.getString("No_Ventas");
                registros[1]=rs.getString("Fecha_Pedido");
                registros[2]=rs.getString("Fecha_Final");
                registros[3]=rs.getString("Cargas");
                registros[4]=rs.getString("Tipo_Servicio");
                registros[5]=rs.getString("Precio_Servicio");
                registros[6]=rs.getString("Estado_Entrega");
                registros[7]=rs.getString("Clientes_Clave_Clientes");
                registros[8]=rs.getString("Trabajador_Clave_Trabajador");
                ModeloTabla.addRow(registros);
            }
            tabla.setModel(ModeloTabla);
            JOptionPane.showMessageDialog(null,"Busqueda Finalizada");
        }catch(SQLException | HeadlessException e){
            JOptionPane.showMessageDialog(null,e,"Error de conexion",JOptionPane.ERROR_MESSAGE);
        }finally{
            if(conect!=null){
               try{
                   conect.close();
               } catch(Exception ex){
                   JOptionPane.showMessageDialog(null,ex,"Error de cierre",JOptionPane.ERROR_MESSAGE);
               }
            }
        }
    }
    
    //Buscar Inventario
    public void Buscari(String dato, String filtro, JTable tabla){
        String [] columnas={"ID_Inventario","Nombre_Producto","Existencia"};
        String [] registros=new String[3];
        ModeloTabla=new DefaultTableModel(null,columnas);
        String SSQL = null;
        Connection conect=null;
        switch (filtro) {
            case "Nombre_Producto":
                SSQL="SELECT ID_Inventario,Nombre_Producto,Existencia "
                        + "FROM inventario WHERE Nombre_Producto LIKE '%"+dato+"%'";
                break;
            case "Existencia":
                SSQL="SELECT ID_Inventario,Nombre_Producto,Existencia "
                        + "FROM inventario WHERE Existencia LIKE '%"+dato+"%'";
                break;        }
        try{
            conect=metodospool.dataSource.getConnection();
            PreparedStatement st = conect.prepareStatement(SSQL);
            ResultSet rs = st.executeQuery();
            while(rs.next()){
                registros[0]=rs.getString("ID_Inventario");
                registros[1]=rs.getString("Nombre_Producto");
                registros[2]=rs.getString("Existencia");
                ModeloTabla.addRow(registros);
            }
            
            tabla.setModel(ModeloTabla);
            JOptionPane.showMessageDialog(null,"Busqueda finalizada");
        }catch(Exception e){
            JOptionPane.showMessageDialog(null,e,"Error de conexion",JOptionPane.ERROR_MESSAGE);
        }finally{
            if(conect!=null){
               try{
                   conect.close();
               } catch(Exception ex){
                   JOptionPane.showMessageDialog(null,ex,"Error de cierre",JOptionPane.ERROR_MESSAGE);
               }
            }
        }   
    }
    
    //Buscar ListaEmpleados
    public void Buscarle(String dato, String filtro, JTable tabla){
        String [] columnas={"Clave_Trabajador","Nombre","Fecha_Nacimiento","Direccion","Telefono","Email","Usuario"};
        String [] registros=new String[7];
        ModeloTabla=new DefaultTableModel(null,columnas);
        String SSQL = null;
        Connection conect=null;
        switch (filtro) {
            case "Nombre":
                SSQL="SELECT Clave_Trabajador,Nombre,Fecha_Nacimiento,Direccion,Telefono,Email,Usuario "
                        + "FROM trabajador WHERE Nombre LIKE '%"+dato+"%'";
                break;
            case "Direccion":
                SSQL="SELECT Clave_Trabajador,Nombre,Fecha_Nacimiento,Direccion,Telefono,Email,Usuario "
                        + "FROM trabajador WHERE Direccion LIKE '%"+dato+"%'";
                break;
            case "Usuario":
                SSQL="SELECT Clave_Trabajador,Nombre,Fecha_Nacimiento,Direccion,Telefono,Email,Usuario "
                        + "FROM trabajador WHERE Usuario LIKE '%"+dato+"%'";
                break;        
        }
        try{
            conect=metodospool.dataSource.getConnection();
            PreparedStatement st = conect.prepareStatement(SSQL);
            ResultSet rs = st.executeQuery();
            while(rs.next()){
                registros[0]=rs.getString("Clave_Trabajador");
                registros[1]=rs.getString("Nombre");
                registros[2]=rs.getString("Fecha_Nacimiento");
                registros[3]=rs.getString("Direccion");
                registros[4]=rs.getString("Telefono");
                registros[5]=rs.getString("Email");
                registros[6]=rs.getString("Usuario");
                ModeloTabla.addRow(registros);
            }
            
            tabla.setModel(ModeloTabla);
            JOptionPane.showMessageDialog(null,"Busqueda finalizada");
        }catch(SQLException | HeadlessException e){
            JOptionPane.showMessageDialog(null,e,"Error de conexion",JOptionPane.ERROR_MESSAGE);
        }finally{
            if(conect!=null){
               try{
                   conect.close();
               } catch(Exception ex){
                   JOptionPane.showMessageDialog(null,ex,"Error de cierre",JOptionPane.ERROR_MESSAGE);
               }
            }
        }   
    }
    //Buscar Vista
    public void Buscarvi(String dato, JTable tabla){
        String [] columnas={"No_Ventas","Fecha_Pedido","Fecha_Final","Cargas","Tipo_Servicio","Precio_Servicio","Estado_Entrega","Clientes_Clave_Clientes","Trabajador_Clave_Trabajador"};
        String [] registros=new String[9];
        ModeloTabla=new DefaultTableModel(null,columnas);
        String SSQL;
        Connection conect = null;
        SSQL="SELECT No_Ventas,Fecha_Pedido,Fecha_Final,Cargas,Tipo_Servicio,Precio_Servicio,Estado_Entrega,Clientes_Clave_Clientes,Trabajador_Clave_Trabajador "
        + "FROM ventas WHERE Trabajador_Clave_Trabajador LIKE '%"+dato+"%'";
        try{
            conect=metodospool.dataSource.getConnection();
            PreparedStatement st = conect.prepareStatement(SSQL);
            ResultSet rs = st.executeQuery();
            while(rs.next()){
                registros[0]=rs.getString("No_Ventas");
                registros[1]=rs.getString("Fecha_Pedido");
                registros[2]=rs.getString("Fecha_Final");
                registros[3]=rs.getString("Cargas");
                registros[4]=rs.getString("Tipo_Servicio");
                registros[5]=rs.getString("Precio_Servicio");
                registros[6]=rs.getString("Estado_Entrega");
                registros[7]=rs.getString("Clientes_Clave_Clientes");
                registros[8]=rs.getString("Trabajador_Clave_Trabajador");                
                ModeloTabla.addRow(registros);
            }
            
            tabla.setModel(ModeloTabla);
            JOptionPane.showMessageDialog(null,"Busqueda finalizada");
        }catch(SQLException | HeadlessException e){
            JOptionPane.showMessageDialog(null,e,"Error de conexion",JOptionPane.ERROR_MESSAGE);
        }finally{
            if(conect!=null){
               try{
                   conect.close();
               } catch(Exception ex){
                   JOptionPane.showMessageDialog(null,ex,"Error de cierre",JOptionPane.ERROR_MESSAGE);
               }
            }
        }   
    }
}
